<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class CommentController extends AbstractController
{
    /**
     * @Route("/comment/create", name="comment_create", methods={"POST"})
     */
    public function create(Request $request, SerializerInterface $serializer): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        if ($request->isXmlHttpRequest()) {
            /** @var Post $post */
            $post = $this
                ->getDoctrine()
                ->getRepository(Post::class)
                ->findOneBy([
                    'id' => $request->get('postId')
                ]);

            if ($post) {
                $comment = new Comment();
                $comment
                    ->setCreatedAt(new \DateTime())
                    ->setCreatedBy($this->getUser())
                    ->setPost($post)
                    ->setText($request->get('comment'));

                $entityManager = $this
                    ->getDoctrine()
                    ->getManager();
                $entityManager->persist($comment);
                $entityManager->flush();

                return $this->json([
                    'id' => $comment->getId()
                ]);
            }
        }

        throw $this->createNotFoundException();
    }
}
