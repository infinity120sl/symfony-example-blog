<?php

namespace App\Controller;

use App\Form\PostType;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PostController
 * @package App\Controller
 *
 * @Route("/post")
 */
class PostController extends AbstractController
{
    private $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * @Route("/create", name="post_create")
     */
    public function create(Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $form = $this->createForm(PostType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            $post->setCreatedBy($this->getUser());
            $post->setCreatedAt(new \DateTime());
            $post->setStatus(['new']);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($post);
            $entityManager->flush();

            return $this->redirectToRoute('post_view', [
                'id' => $post->getId()
            ]);
        }

        return $this->render('post/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id<\d>}", name="post_view")
     */
    public function view(int $id)
    {
        $post = $this->postRepository->findOneBy([
            'id' => $id
        ]);

        if (!$post) {
            return $this->createNotFoundException();
        }

        return $this->render('post/view.html.twig', [
            'post' => $post
        ]);
    }

    /**
     * @Route("/delete/{id<\d>}", name="post_delete")
     */
    public function delele(int $id)
    {
        $post = $this->postRepository->findOneBy([
            'id' => $id
        ]);

        if ($post) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($post);
            $entityManager->flush();
        }

        return $this->redirectToRoute('main_page');
    }
}
